class Board():
    def __init__(self):
        self.state = [
            ' ', ' ', ' ', 
            ' ', ' ', ' ', 
            ' ', ' ', ' ', 
        ]

    def is_open(self, i):
        return self.state[i] == ' '

    def move(self, i, token):
        self.state[i] = token

    def get_turn(self):
        opens = filter(lambda x: x == ' ', self.state)
        return 9 - len(list(opens)) + 1

    def print(self):
        print(f' {self.state[0]} | {self.state[1]} | {self.state[2]} ')
        print('-----------')
        print(f' {self.state[3]} | {self.state[4]} | {self.state[5]} ')
        print('-----------')
        print(f' {self.state[6]} | {self.state[7]} | {self.state[8]} ')
