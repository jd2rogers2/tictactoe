from board import Board

wins = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
]

class Game():
    def __init__(self):
        self.board = Board()

    def is_tie(self):
        taken_spaces = filter(lambda space: space != ' ', self.board.state)
        return len(list(taken_spaces)) == 9

    def is_win(self):
        for win in wins:
            if (
                self.board.state[win[0]] == self.board.state[win[1]]
                and self.board.state[win[0]] == self.board.state[win[2]]
                and self.board.state[win[0]] != ' '
            ):
                return self.board.state[win[0]]
        return False

    def is_over(self):
        if (
            self.is_tie()
            or self.is_win() != False
        ):
            return True
        return False

    def turn(self):
        self.board.print()

        turn_num = self.board.get_turn()
        token = 'X' if turn_num % 2 != 0 else 'O'
        output = input(f'Go {token}! -> ')
        move = int(output) if output else False

        if (
            type(move) == int
            and move in range(1, 10)
            and self.board.is_open(move - 1)
        ):
            self.board.move(move - 1, token)
        else:
            self.turn()

    def play(self):
        print('let\'s play some tic tac toe!')

        while self.is_over() == False:
            self.turn()

        if self.is_win() != False:
            print(f'{self.is_win()} is the winner!')
        elif self.is_tie():
            print('Tie ball game T-T')
